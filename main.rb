def LoadFile(path)
	numbers = Array.new

	File.foreach(path) { |line| #excludes blank lines and non numbers
	  numbers.push line.to_f unless line.chomp.empty? || !Float(line) rescue false
	}

	return numbers
end

def CalculateAverage(numbers)
	average = 0
	numbers.each{ |n|
		average += n
	}
	average = average / numbers.length
end

numbers = LoadFile(ARGV[0])
puts "Average: #{CalculateAverage(numbers)}"