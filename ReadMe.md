# Average Calculator

Welcome to my average calculator written in Ruby.
Run the program with a command like this:"ruby main.rb data.csv"

Make sure to include the data file in the same directory.
The program loads files with numbers seperated by a new line not commas, despite the included data file being labled a csv.

Thanks for reading. 